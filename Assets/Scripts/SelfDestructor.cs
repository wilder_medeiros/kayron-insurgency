﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructor : MonoBehaviour
{
    [SerializeField] float destroyDelay = 5f;

    void Start()
    {
        Destroy(gameObject, 5f);
    }
}
