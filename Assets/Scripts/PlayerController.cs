﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [Tooltip("In ms^-1")][SerializeField] float xControlSpeed = 20f;
    [Tooltip("In ms^-1")] [SerializeField] float yControlSpeed = 10f;
    [SerializeField] GameObject[] guns;

    [Header("Screen Range")]
    [Tooltip("In m")][SerializeField] float xRange = 15f;
    [Tooltip("In m")][SerializeField] float yRange = 10f;

    [Header("Screen-position Based")]
    [SerializeField] float positionPitchFactor = -1f;
    [SerializeField] float positionYawFactor = 1.5f;

    [Header("Control-throw Based")]
    [SerializeField] float controlPitchFactor = -10f;
    [SerializeField] float controlRollFactor = -20f;

    AudioSource audioSource;

    float xThrow, yThrow;
    bool isControlEnabled = true;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    private void ProcessRotation()
    {
        float pitchToDuePosition = transform.localPosition.y * positionPitchFactor;
        float pitchToDueControlThrow = yThrow * controlPitchFactor;
        float pitch = pitchToDuePosition + pitchToDueControlThrow;

        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor; 

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");

        float xOffset = xThrow * xControlSpeed * Time.deltaTime;
        float yOffset = yThrow * yControlSpeed * Time.deltaTime;

        float rawNewXPos = transform.localPosition.x + xOffset;
        float clampedXPos = Mathf.Clamp(rawNewXPos, -xRange, xRange);

        float rawNewYPos = transform.localPosition.y + yOffset;
        float clampedYPos = Mathf.Clamp(rawNewYPos, -yRange, yRange);


        transform.localPosition = new Vector3(clampedXPos, clampedYPos, transform.localPosition.z);
    }

    private void OnPlayerDeath() // Called from a string message on collisionHandler script
    {
        isControlEnabled = false;
        print("player dying");
    }

    private void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            SetGunsActive(true);
            PlaySoundEffect();
        } else
        {
            SetGunsActive(false);
            audioSource.Stop();
        }
    }

    private void PlaySoundEffect()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    private void SetGunsActive(bool isActive)
    {
        foreach (GameObject gun in guns)
        {
            var emissionModule = gun.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = isActive;
        }
    }
}
