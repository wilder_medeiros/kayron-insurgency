﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class LevelHandler : MonoBehaviour
{
    void Start()
    {
        
    }

    
    void Update()
    {
        if (CrossPlatformInputManager.GetButton("Fire2"))
        {
            Application.Quit();
            Debug.Log("Saiu");
        }
    }

    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (SceneManager.sceneCountInBuildSettings == nextSceneIndex)
        {
            nextSceneIndex = 0; // loop back to start 
        }
        SceneManager.LoadScene(nextSceneIndex);
    }

    public void ApplicationQuit()
    {
        Application.Quit();
    }


}
